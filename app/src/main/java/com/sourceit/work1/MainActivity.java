package com.sourceit.work1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editText)
    EditText inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportFragmentManager().beginTransaction()
                .replace((R.id.container,BlankFragment.newInstance("name"))
                .addToBackStack(null)
                .commit();

    }


    @OnClick(R.id.button)
    public void OnPresentClick()

    {
        Toast.makeText(getApplicationContext(),
                inputText.getText().toString(), Toast.LENGTH_SHORT);


    }

    @OnTextChanged(R.id.editText)
    public void OnChange() {
        Toast.makeText(getApplicationContext(),
                inputText.getText().toString(), Toast.LENGTH_SHORT).show();


    }
}
